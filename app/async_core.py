import os
import json

from rq import Queue
from worker import conn
from .arena_api import get_random_block, get_backup_block

random_block_queue = Queue('random_arena_blocks', connection=conn)

min_blocks = int(os.getenv('MIN_BLOCKS', '10'))

random_block_list = 'random_arena_blocks_list'

def save_random_block():
    block_str = json.dumps(get_random_block())
    conn.lpush(random_block_list, block_str)

def get_random_block_from_cache():
    """Get a random block from redis list and create job to replace it"""
    if conn.llen(random_block_list) < min_blocks:
        random_block_queue.enqueue(save_random_block)
        random_block_queue.enqueue(save_random_block)
    else: 
        random_block_queue.enqueue(save_random_block)

    raw_block =  conn.rpop(random_block_list)
    if raw_block is None:
        return get_backup_block()
    return json.loads(raw_block)
