import os
from functools import wraps
from flask import Flask, session, redirect, url_for, request 
from markupsafe import escape

from app.async_core import get_random_block_from_cache

from app import app

@app.route('/random-block/v1/block', methods=['GET'])
def random_block():
    return {
        'block': get_random_block_from_cache()
    }