import os
import json
import random
import pprint

import requests

arena_key = os.environ['ARENA_TOKEN']
arena_user = os.environ['ARENA_USER']
base_url = 'http://api.are.na/v2'

def _call_arena(endpoint, params):
    resp = requests.get(
        f"{base_url}{endpoint}",
        params=params or {},
        headers={
            'Authorization': 'Bearer {}'.format(arena_key)
        }
    )
    if resp.status_code != 200:
        resp.raise_for_status()
    return resp.json()

def _get_user_channels():
    raw_resp = _call_arena(f"/users/{arena_user}/channels", { 'per': '100'} )
    return raw_resp['channels']

def get_random_block():
    """Query arena api for a random block from the user"""
    channels = _get_user_channels()
    non_private_channels = [chan for chan in channels if chan['status'] != 'private']
    weights = [chan['length'] for chan in non_private_channels]
    random_channel = random.choices(non_private_channels, weights=weights, k=1)[0]
    random_channel_slug = random_channel['slug']
    print(f"channel title: {random_channel['title']}")
    blocks_per_page = 10
    page_count = random_channel['length'] / blocks_per_page
    random_page_index = random.randint(0, int(page_count))
    
    blocks = _call_arena(f"/channels/{random_channel_slug}/contents", { 'per': blocks_per_page, 'page': random_page_index } )
    blocks_with_out_channels = [b for b in blocks['contents'] if b['base_class'] == 'Block']
    random_block = random.choice(blocks_with_out_channels)
    print(f"block title: {random_block['title']}")
    return random_block

def get_backup_block():
    """Load block json from file instead of from arena API"""
    with open('backup_block.json', 'r') as json_file:
        block = json.load(json_file)
        return block