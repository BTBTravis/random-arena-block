# Random Arena Block

Little python flask api to return a random block from a are.na user


## Dev Scripts

Run Dev Server with:

```shell
FLASK_APP=random-arena-block.py flask run -p 5001 --reload
```

Run Worker with:

```shell
python worker.py
```

Run redix gui explorer with:

```shell
$ npx redis-commander
```

## Dev Links

- https://pythontic.com/database/redis/list
- https://www.npmjs.com/package/redis-commander
- https://devcenter.heroku.com/articles/python-rq
- https://redis-py.readthedocs.io/en/stable/index.html#redis.Redis.blpop
- https://redis.io/commands/lpop
- http://python-rq.org/docs/